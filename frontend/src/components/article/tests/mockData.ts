import { ArticleProps, CommentProps } from '../../../types'

export const mockArticle: ArticleProps = {
  slug: 'test-article',
  title: 'Test Article',
  author: {
    username: 'testuser',
    following: false,
    bio: 'This is a test user.',
    image: 'test-article.jpg'
  },
  body: 'This is a test article.',
  tagList: ['test', 'article'],
  createdAt: '2022-01-01T00:00:00Z',
  favorited: false,
  favoritesCount: 0,
  description: 'This is a test article.'
}

export const mockComment: CommentProps = {
  id: 1,
  body: 'Test comment',
  author: {
    username: 'testuser',
    following: false,
    bio: 'This is a test user.',
    image: 'test-comment.jpg'
  },
  createdAt: '2022-01-01T00:00:00Z'
}

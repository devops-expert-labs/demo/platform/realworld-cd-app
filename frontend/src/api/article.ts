import { GET, POST, DELETE, PUT } from './config'

interface articleAPIBodyProps {
  article: {
    title: string
    description: string
    body: string
    tagList: string[]
  }
}

export const getArticles = (query: string, signal: AbortSignal): Promise<any> =>
  new Promise((resolve, reject) => {
    const articlesData = {
      articles: [
        {
          slug: 'the-benefits-of-regular-exercise-15',
          title: 'The Benefits of Regular Exercise',
          description: 'Discover the physical and mental benefits of regular exercise',
          body: 'Regular exercise is essential for maintaining good physical and mental health. In this article, we will explore the many benefits of exercise and provide practical tips and techniques for incorporating it into your daily routine.',
          tagList: ['Regular Exercise'],
          createdAt: '2022-05-01T17:00:00.000+09:00',
          updatedAt: '2022-05-01T17:00:00.000+09:00',
          favorited: false,
          favoritesCount: 1,
          author: {
            username: 'johndoe',
            bio: null,
            image: 'https://fastly.picsum.photos/id/870/128/128.jpg?hmac=2ZV4rAlk84KkKihP8rQmhwGnLuz4XHQDNqTptS93gsg',
            following: false
          }
        },
        {
          slug: 'the-benefits-of-gratitude-journaling-20',
          title: 'The Benefits of Gratitude Journaling',
          description: 'Learn how to cultivate a sense of gratitude and improve your well-being through journaling',
          body: 'Gratitude journaling is a practice that can help you become more aware of the things you are thankful for and cultivate a greater sense of well-being. In this article, we will explore the benefits of gratitude journaling and provide practical tips and techniques for incorporating it into your daily routine.',
          tagList: ['Journaling'],
          createdAt: '2022-05-01T17:00:00.000+09:00',
          updatedAt: '2022-05-01T17:00:00.000+09:00',
          favorited: false,
          favoritesCount: 1,
          author: {
            username: 'janedoe',
            bio: null,
            image: 'https://fastly.picsum.photos/id/348/128/128.jpg?hmac=rVEm96iU_fNyKji6ZuQGe6cjZjp6xGMnY9ganlJebew',
            following: false
          }
        },
        {
          slug: 'the-benefits-of-a-daily-gratitude-journal-25',
          title: 'The Benefits of a Daily Gratitude Journal',
          description:
            'Learn how to cultivate gratitude and improve your well-being through a daily journaling practice',
          body: 'Keeping a daily gratitude journal is a powerful practice that can help you become more aware of the things you are thankful for and cultivate a greater sense of well-being. In this article, we will explore the benefits of a daily gratitude journal and provide practical tips and techniques for incorporating it into your daily routine.',
          tagList: ['Gratitude', 'Journaling'],
          createdAt: '2022-05-01T17:00:00.000+09:00',
          updatedAt: '2022-05-01T17:00:00.000+09:00',
          favorited: false,
          favoritesCount: 0,
          author: {
            username: 'bobsmith',
            bio: null,
            image: 'https://fastly.picsum.photos/id/662/128/128.jpg?hmac=QVOT0StEK4Toi7b2WqqTQihE2XKic3foUjqm2YyggDc',
            following: false
          }
        },
        {
          slug: 'the-art-of-journaling-19',
          title: 'The Art of Journaling',
          description: 'Learn how to reflect on your experiences and gain insights about yourself through journaling',
          body: 'Journaling is a powerful tool that can help you gain insights about yourself, your values, and your goals. In this article, we will explore the art of journaling and provide practical tips and techniques for incorporating it into your daily routine.',
          tagList: ['Journaling'],
          createdAt: '2022-04-01T12:15:00.000+09:00',
          updatedAt: '2022-04-01T12:15:00.000+09:00',
          favorited: false,
          favoritesCount: 0,
          author: {
            username: 'janedoe',
            bio: null,
            image: 'https://fastly.picsum.photos/id/348/128/128.jpg?hmac=rVEm96iU_fNyKji6ZuQGe6cjZjp6xGMnY9ganlJebew',
            following: false
          }
        },
        {
          slug: 'how-to-overcome-procrastination-14',
          title: 'How to Overcome Procrastination',
          description: 'Learn how to stop procrastinating and get things done',
          body: 'Procrastination can be a major obstacle to achieving your goals. In this article, we will explore the causes of procrastination and provide practical tips and techniques for overcoming it and getting things done.',
          tagList: ['Procrastination'],
          createdAt: '2022-04-01T12:15:00.000+09:00',
          updatedAt: '2022-04-01T12:15:00.000+09:00',
          favorited: false,
          favoritesCount: 0,
          author: {
            username: 'johndoe',
            bio: null,
            image: 'https://fastly.picsum.photos/id/870/128/128.jpg?hmac=2ZV4rAlk84KkKihP8rQmhwGnLuz4XHQDNqTptS93gsg',
            following: false
          }
        },
        {
          slug: 'the-power-of-affirmations-24',
          title: 'The Power of Affirmations',
          description: 'Learn how to use affirmations to cultivate a more positive mindset and achieve your goals',
          body: 'Affirmations are positive statements that can help you overcome negative thinking patterns, cultivate a more positive mindset, and achieve your goals. In this article, we will explore the power of affirmations and provide practical tips and techniques for incorporating them into your daily routine.',
          tagList: ['Affirmations'],
          createdAt: '2022-04-01T12:15:00.000+09:00',
          updatedAt: '2022-04-01T12:15:00.000+09:00',
          favorited: false,
          favoritesCount: 0,
          author: {
            username: 'bobsmith',
            bio: null,
            image: 'https://fastly.picsum.photos/id/662/128/128.jpg?hmac=QVOT0StEK4Toi7b2WqqTQihE2XKic3foUjqm2YyggDc',
            following: false
          }
        },
        {
          slug: 'the-benefits-of-a-gratitude-practice-23',
          title: 'The Benefits of a Gratitude Practice',
          description: 'Learn how to cultivate gratitude and improve your well-being through a daily practice',
          body: 'Cultivating gratitude is a powerful practice that can help you reduce stress, increase happiness, and improve your overall well-being. In this article, we will explore the benefits of a gratitude practice and provide practical tips and techniques for incorporating it into your daily routine.',
          tagList: ['Gratitude'],
          createdAt: '2022-03-01T08:45:00.000+09:00',
          updatedAt: '2022-03-01T08:45:00.000+09:00',
          favorited: false,
          favoritesCount: 0,
          author: {
            username: 'bobsmith',
            bio: null,
            image: 'https://fastly.picsum.photos/id/662/128/128.jpg?hmac=QVOT0StEK4Toi7b2WqqTQihE2XKic3foUjqm2YyggDc',
            following: false
          }
        },
        {
          slug: 'the-benefits-of-mindful-eating-18',
          title: 'The Benefits of Mindful Eating',
          description: 'Learn how to improve your relationship with food and cultivate healthier eating habits',
          body: 'Mindful eating is a practice that can help you become more aware of your food choices and develop a healthier relationship with food. In this article, we will explore the benefits of mindful eating and provide practical tips and techniques for incorporating it into your daily routine.',
          tagList: ['Mindful Eating'],
          createdAt: '2022-03-01T08:45:00.000+09:00',
          updatedAt: '2022-03-01T08:45:00.000+09:00',
          favorited: false,
          favoritesCount: 1,
          author: {
            username: 'janedoe',
            bio: null,
            image: 'https://fastly.picsum.photos/id/348/128/128.jpg?hmac=rVEm96iU_fNyKji6ZuQGe6cjZjp6xGMnY9ganlJebew',
            following: false
          }
        },
        {
          slug: 'the-benefits-of-mindfulness-meditation-13',
          title: 'The Benefits of Mindfulness Meditation',
          description: 'Learn how to reduce stress and increase focus with mindfulness meditation',
          body: 'Mindfulness meditation is a powerful practice that can help you reduce stress, increase focus, and cultivate a greater sense of well-being. In this article, we will explore the benefits of mindfulness meditation and provide practical tips and techniques for incorporating it into your daily routine.',
          tagList: ['Meditation'],
          createdAt: '2022-03-01T08:45:00.000+09:00',
          updatedAt: '2022-03-01T08:45:00.000+09:00',
          favorited: false,
          favoritesCount: 1,
          author: {
            username: 'johndoe',
            bio: null,
            image: 'https://fastly.picsum.photos/id/870/128/128.jpg?hmac=2ZV4rAlk84KkKihP8rQmhwGnLuz4XHQDNqTptS93gsg',
            following: false
          }
        }
      ],
      articlesCount: 9
    }
    if (articlesData) {
      resolve(articlesData)
    } else {
      reject('Failed to fetch articles')
    }
  })

export const postArticle = (body: articleAPIBodyProps) => POST('/articles', body)

export const getArticle = (slug: string) => GET(`/articles/${slug}`)

export const putArticle = (slug: string, body: articleAPIBodyProps) => PUT(`/articles/${slug}`, body)

export const deleteArticle = (slug: string) => DELETE(`/articles/${slug}`)

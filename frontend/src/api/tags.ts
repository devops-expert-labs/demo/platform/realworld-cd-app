export const getTags = (): Promise<any> =>
  new Promise((resolve, reject) => {
    const data = {
      tags: [
        'Time Management',
        'Positive Thinking',
        'Meditation',
        'Procrastination',
        'Regular Exercise',
        'Yoga',
        'Gratitude',
        'Mindful Eating',
        'Journaling',
        'Deep Breathing',
        'Visualization',
        'Affirmations'
      ]
    }
    if (data) {
      resolve(data)
    } else {
      reject('Failed to fetch articles')
    }
    if (data) {
      resolve(data)
    } else {
      reject('Failed to fetch articles')
    }
  })

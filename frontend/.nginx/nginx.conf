## https://nginx.org/en/docs/dirindex.html
## https://nginx.org/en/docs/ngx_core_module.html
## https://nginx.org/en/docs/http/ngx_http_core_module.html
## https://nginx.org/en/docs/http/ngx_http_log_module.html

# Auto detects a good number of processes to run
worker_processes auto; ## Default: 1

# Provides the configuration file context in which the directives that affect connection processing are specified.
events {
    # Sets the maximum number of simultaneous connections that can be opened by a worker process.
    worker_connections 4096;  ## Default: 1024
    # Tells the worker to accept multiple connections at a time
    multi_accept on;
}

# Provides the configuration file context in which the HTTP server directives are specified.
http {
    include       /etc/nginx/mime.types;
    index         index.html;

    # Default MIME type
    default_type  application/octet-stream;

    log_format compression '$remote_addr - $remote_user [$time_local] '
        '"$request" $status $upstream_addr '
        '"$http_referer" "$http_user_agent"';

    # Sets configuration for a virtual server.
    server {
        listen 80;
        access_log /var/log/nginx/access.log compression;

        root /var/www;

        location / {
            # First attempt to serve request as file, then
            # as directory, then fall back to redirecting to index.html
            try_files $uri $uri/ /index.html;
        }

        # Media: images, icons, video, audio, HTC
        location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
          expires 1M;
          access_log off;
          add_header Cache-Control "public";
        }

        # Javascript and CSS files
        location ~* \.(?:css|js)$ {
            try_files $uri =404;
            expires 1y;
            access_log off;
            add_header Cache-Control "public";
        }

        # Any route containing a file extension (e.g. /devicesfile.js)
        location ~ ^.+\..+$ {
            try_files $uri =404;
        }
    }
}
